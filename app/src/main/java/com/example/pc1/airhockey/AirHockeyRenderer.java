package com.example.pc1.airhockey;

import android.content.Context;
import android.graphics.Color;
import android.opengl.GLSurfaceView.Renderer;

import com.example.pc1.opengl_1.R;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import static android.opengl.GLES20.*;
import static android.opengl.GLUtils.*;
import static android.opengl.Matrix.*;
import java.nio.FloatBuffer;

import util.LoggerConfig;
import util.MatrixHelper;
import util.ShaderHelper;
import util.TextResourceReader;

/**
 * Created by PC1 on 11/26/2016.
 */
public class AirHockeyRenderer implements Renderer {

    private static final int POSITION_COMPONENT_COUNT = 4;

    private static final int BYTES_PER_FLOAT = 4;

    // Attribute Color
    private static final String A_COLOR = "a_Color";
    private static final int COLOR_COMPONENT_COUNT = 3;
    private static final int STRIDE =
            (POSITION_COMPONENT_COUNT + COLOR_COMPONENT_COUNT) * BYTES_PER_FLOAT;
    private int aColorLocation;

    // Uniform Color
    private static final String U_COLOR = "u_Color";
    private int uColorLocation;

    // Position
    private static final String A_POSITION = "a_Position";
    private int aPositionLocation;

    private final FloatBuffer vertexData;

    private final Context parentContext;

    private int program;

    // Projection Matrix
    private static final String U_MATRIX = "u_Matrix";
    private final float[] projectionMatrix = new float[16];
    private int uMatrixLocation;

    // Model matrix for translations
    private final float[] modelMatrix = new float[16];

    public AirHockeyRenderer(Context context) {
        this.parentContext = context;
        float[] tableVerticesWithTriangles = {
                // X, Y, R, G, B

                // Triangle Fan
                //  X      Y    Z     W      R     G     B
                   0f,    0f,  0f, 1f,    1f,   1f,   1f,
                -0.5f, -0.8f,  0f,   1f,  0.7f, 0.7f, 0.7f,
                 0.5f, -0.8f,  0f,   1f,  0.7f, 0.7f, 0.7f,
                 0.5f,  0.8f,  0f,   1f,  0.7f, 0.7f, 0.7f,
                -0.5f,  0.8f,  0f,   1f,  0.7f, 0.7f, 0.7f,
                -0.5f, -0.8f,  0f,   1f,  0.7f, 0.7f, 0.7f,

                // Line 1
                -0.5f,    0f,  0f, 1f,  0.5f, 0.0f, 0.0f,
                   0f,    0f,  0f, 1f,  0.9f, 0.0f, 0.0f,
                 0.5f,    0f,  0f, 1f,  0.5f, 0.0f, 0.0f,

                // Mallets
                   0f,-0.4f,   0f,1f,    0f,   0f,   1f,
                   0f, 0.4f,   0f,1f,    1f,   0f,   0f
        };
        vertexData = ByteBuffer
                .allocateDirect(tableVerticesWithTriangles.length * BYTES_PER_FLOAT)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();
        vertexData.put(tableVerticesWithTriangles);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        glClearColor(0.2f, 0.2f, 0.25f, 1.0f);
        String vertexShaderSource =
                TextResourceReader.readTextFileFromResource(parentContext,
                        R.raw.simple_vertex_shader);
        String fragmentShaderSource =
                TextResourceReader.readTextFileFromResource(parentContext,
                        R.raw.simple_fragment_shader);
        // Let's compile now
        int vertexShader = ShaderHelper.compileVertexShader(vertexShaderSource);
        int fragmentShader = ShaderHelper.compileFragmentShader(fragmentShaderSource);
        // And Link
        this.program = ShaderHelper.linkProgram(vertexShader, fragmentShader);

        if (LoggerConfig.On){
            ShaderHelper.validateProgram(this.program);
        }
        // Finally use the program
        glUseProgram(this.program);

//        uColorLocation = glGetUniformLocation(program, U_COLOR);
        aColorLocation = glGetAttribLocation(program, A_COLOR);
        aPositionLocation = glGetAttribLocation(program, A_POSITION);
        uMatrixLocation = glGetUniformLocation(program, U_MATRIX);


        // FOR POSITION --------
        // Each buffer has an internal pointer that can be moved by calling this to start from 0
        vertexData.position(0);
        // Tell OpenGL that it can find data for a_Position in the buffer vertexData
        glVertexAttribPointer(aPositionLocation, POSITION_COMPONENT_COUNT, GL_FLOAT, false,
                STRIDE, vertexData);
        glEnableVertexAttribArray(aPositionLocation);

        // FOR COLOR VALUES --------
        vertexData.position(POSITION_COMPONENT_COUNT);
        glVertexAttribPointer(aColorLocation, COLOR_COMPONENT_COUNT, GL_FLOAT, false,
                STRIDE, vertexData);
        glEnableVertexAttribArray(aColorLocation);

    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        glViewport(0, 0, width, height);

        // Determine which is greater and divide by smaller to get aspecst ratio
//        final float aspectRatio = (width > height) ?
//                (float)width / (float)height :
//                (float)height / (float)width;

//        if (width > height)
//            orthoM(projectionMatrix, 0, -aspectRatio, aspectRatio, -1f, 1f, -1f, 1f);   // Landscape
//        else
//            orthoM(projectionMatrix, 0, -1f, 1f, -aspectRatio, aspectRatio, -1f, 1f);   // Portrait

        MatrixHelper.perspectiveM(projectionMatrix, 45, (float)width/(float)height, 1f, 10f);
        setIdentityM(modelMatrix, 0);   // Create identity matrix
        translateM(modelMatrix, 0, 0f, 0f, -3f);
        rotateM(modelMatrix, 0, -60f, 1f, 0f, 0f);

        final float[] tempMatrix = new float[16];
        multiplyMM(tempMatrix, 0, projectionMatrix, 0, modelMatrix, 0);
        System.arraycopy(tempMatrix, 0, projectionMatrix, 0, tempMatrix.length);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUniformMatrix4fv(uMatrixLocation, 1, false, projectionMatrix, 0);

        // Table triangles
        // Update uniform color vector variable
        // Start drawing triangles from 0 position and take 6 vertices from vertexData

        // Background Border Triangles
//        glUniform4f(uColorLocation, 0.6f, 0.6f, 0.6f, 1.0f);
//        glDrawArrays(GL_TRIANGLES, 0, 6);

        // Board Triangles
//        glUniform4f(uColorLocation, 0.9f, 0.9f, 0.9f, 1.0f);
//        glDrawArrays(GL_TRIANGLES, 6, 6);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 6);

        // Draw line
//        glUniform4f(uColorLocation, 0.3f, 0.0f, 0.0f, 1.0f);
        glLineWidth(10);
        glDrawArrays(GL_LINES, 6, 2);
        glDrawArrays(GL_LINES, 7, 2);

        // Draw First Mallet
//        glUniform4f(uColorLocation, 0.2f, 0.2f, 0.7f, 1.0f);
        glDrawArrays(GL_POINTS, 9, 1);


        // Draw Second Mallet
//        glUniform4f(uColorLocation, 0.7f, 0.2f, 0.2f, 1.0f);
        glDrawArrays(GL_POINTS, 10, 1);
    }
}
