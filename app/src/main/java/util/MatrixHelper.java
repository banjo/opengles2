package util;

/**
 * Created by redpix on 12/20/2016.
 */

public class MatrixHelper {

    public static void perspectiveM(float[] perspectiveMatrix, float yFovInDegrees, float aspectRatio,
                                    float near, float far){
        final float angleInRadians = (float) (yFovInDegrees * Math.PI / 180.0f);
        final float focalLength = (float) (1.0 / Math.tan(angleInRadians / 2.0f));

        perspectiveMatrix[0] = focalLength/aspectRatio;
        perspectiveMatrix[1] = 0f;
        perspectiveMatrix[2] = 0f;
        perspectiveMatrix[3] = 0f;

        perspectiveMatrix[4] = 0f;
        perspectiveMatrix[5] = focalLength;
        perspectiveMatrix[6] = 0f;
        perspectiveMatrix[7] = 0f;

        perspectiveMatrix[8] = 0f;
        perspectiveMatrix[9] = 0f;
        perspectiveMatrix[10] = -((far + near)/(far - near));
        perspectiveMatrix[11] = -1f;

        perspectiveMatrix[12] = 0f;
        perspectiveMatrix[13] = 0f;
        perspectiveMatrix[14] = -((2 * far * near)/(far - near));
        perspectiveMatrix[15] = 0f;
    }

}
