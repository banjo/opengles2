package util;

import android.util.Log;

import static android.opengl.GLES20.GL_COMPILE_STATUS;
import static android.opengl.GLES20.GL_FRAGMENT_SHADER;
import static android.opengl.GLES20.GL_LINK_STATUS;
import static android.opengl.GLES20.GL_VALIDATE_STATUS;
import static android.opengl.GLES20.GL_VERTEX_SHADER;
import static android.opengl.GLES20.glAttachShader;
import static android.opengl.GLES20.glCompileShader;
import static android.opengl.GLES20.glCreateProgram;
import static android.opengl.GLES20.glCreateShader;
import static android.opengl.GLES20.glDeleteProgram;
import static android.opengl.GLES20.glDeleteShader;
import static android.opengl.GLES20.glGetProgramInfoLog;
import static android.opengl.GLES20.glGetProgramiv;
import static android.opengl.GLES20.glGetShaderInfoLog;
import static android.opengl.GLES20.glGetShaderiv;
import static android.opengl.GLES20.glLinkProgram;
import static android.opengl.GLES20.glShaderSource;
import static android.opengl.GLES20.glValidateProgram;

/**
 * Created by redpix on 11/28/2016.
 */

public class ShaderHelper {
    private static final String TAG = "ShaderHelper";

    public static int compileVertexShader(String shaderCode){
        return compileShader(GL_VERTEX_SHADER, shaderCode);
    }

    public static int compileFragmentShader(String shaderCode){
        return compileShader(GL_FRAGMENT_SHADER, shaderCode);
    }

    public static int compileShader(int type, String shaderCode){
        final int shaderObjectId = glCreateShader(type);

        // Check if shader creation was successful
        if (shaderObjectId == 0){
            if (LoggerConfig.On){
                Log.w(TAG, "Couldn't Create new Shader!");
            }
            return 0;
        }

        glShaderSource(shaderObjectId, shaderCode); // Upload
        glCompileShader(shaderObjectId);            // Attempt to compile

        // Check the gl compile status
        final int[] compileStatus = new int[1];
        glGetShaderiv(shaderObjectId, GL_COMPILE_STATUS, compileStatus, 0);

        if (LoggerConfig.On){
            Log.v(TAG, "Results of Compiling source: " + "\n" + shaderCode + "\n" +
                glGetShaderInfoLog(shaderObjectId));
        }

        if (compileStatus[0] == 0){
            // If it failed delete the shader object
            glDeleteShader(shaderObjectId);
            if (LoggerConfig.On){
                Log.w(TAG, "Compilation of shader failed.");
            }
            return 0;
        }
        // If compilation is finally successful return the shader object id
        return shaderObjectId;
    }

    public static int linkProgram(int vertexShader, int fragmentShader){
        final int programObjectId = glCreateProgram();

        if (programObjectId == 0){
            if (LoggerConfig.On){
                Log.w(TAG, "Could not create new program.");
            }
            return 0;
        }

        // Attach shaders to the created program
        glAttachShader(programObjectId, vertexShader);
        glAttachShader(programObjectId, fragmentShader);

        // Link them
        glLinkProgram(programObjectId);

        // Link status
        final int[] linkStatus = new int[1];
        glGetProgramiv(programObjectId, GL_LINK_STATUS, linkStatus, 0);
        if (LoggerConfig.On){
            // Print the program log info to the android log output
            Log.v(TAG, "Results of linking program: \n" + glGetProgramInfoLog(programObjectId));
        }

        if (linkStatus[0] == 0){
            glDeleteProgram(programObjectId);
            if (LoggerConfig.On){
                Log.w(TAG, "Linking of program failed.");
            }
            return 0;
        }

        return programObjectId;
    }

    public static boolean validateProgram(int programObjectId){
        glValidateProgram(programObjectId);

        // Validate status
        final int[] validateStatus = new int[1];
        glGetProgramiv(programObjectId, GL_VALIDATE_STATUS, validateStatus, 0);

        Log.v(TAG, "Results of validating program: " + validateStatus[0]
            + "\nLog: " + glGetProgramInfoLog(programObjectId));

        return validateStatus[0] != 0;
    }
}
