package util;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by redpix on 11/28/2016.
 */

public class TextResourceReader {
    /**
     * Reads and returns text contents of the given resource file
     * @param context
     * @param resourceId
     * @return String body of the file
     */
    @NonNull
    public static String readTextFileFromResource(Context context, int resourceId){
        StringBuilder body = new StringBuilder();
        try{
            // Read the raw text resource
            InputStream inputStream = context.getResources().openRawResource(resourceId);
            // Create new Input Stream Reader
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            // Create BufferedReader from Input Stream Reader
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            String nextLine;

            while ((nextLine = bufferedReader.readLine()) != null){
                body.append(nextLine);
                body.append("\n");
            }
        } catch (IOException e){
            throw new RuntimeException("Couldn't Open Resource " + resourceId, e);
        } catch (Resources.NotFoundException nfe){
            throw new RuntimeException("Resource Not Found " + resourceId, nfe);
        }
        return body.toString();
    }
}
