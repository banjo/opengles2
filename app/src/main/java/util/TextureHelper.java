package util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.util.logging.Logger;

import static android.opengl.GLES20.*;
import static android.opengl.GLUtils.texImage2D;

/**
 * Created by redpix on 12/23/2016.
 */
public class TextureHelper {
    private final static String TAG = "TextureHelper";

    public static int loadTexture(Context context, int resourceId) {
        final int[] textureObjectIds = new int[1];
        glGenTextures(1, textureObjectIds, 0);

        if (textureObjectIds[0] == 0){
            if (LoggerConfig.On){
                Log.w(TAG, "Could not generate a new OpenGL Texture object");
            }
            return 0;
        }

        // We need to decompress png, jpg files to get raw format for OpenGL to use
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;

        final Bitmap bitmap = BitmapFactory.decodeResource(
                context.getResources(),
                resourceId,
                options);

        if (bitmap == null){
            if (LoggerConfig.On){
                Log.w(TAG, "Resource ID: " + resourceId + " could not be decoded!");
            }

            glDeleteTextures(1, textureObjectIds, 0);
        }

        // If succeeded we tell OpenGL that future texture calls should be applied to this texture
        glBindTexture(GL_TEXTURE_2D, textureObjectIds[0]);

        // Texture filtering modes: nearest-neighbour, bilinear interpolation

        // Nearest-Neighbour Filtering
        //      Selects the nearest texel for each fragment. When we magnify the texture, it will
        //      look rather blocky. Each texel is clearly visible as a small square. When we minify
        //      the texture, many of the details will be lost, as we don't have enough fragment for
        //      all of the texels

        // Bilinear Filtering
        //      Bilinear filtering is used to smooth the transition between pixels. Instead of using
        //      nearest texel for each fragment, OpenGL will use the four neighbouring texels and
        //      interpolate them together using the same type of linear interpolation. We call it
        //      bilinear because it's done along two dimensions.

        // Mipmapping
        //      While bilinear filtering works well for magnification, it doesn't work as well for
        //      minification beyond a certain size. The more we reduce the size of a texture on the
        //      rendering surfacem the more texels will get crammed onto each fragment. Since OpenGL
        //      bilinear filtering will only use 4 texels for each fragment, we still lose a lot of
        //      detail. This can cause noise and shimmering artifacts with moving objects as
        //      different texels get selected with each frame. To combat this artifacts we can use
        //      mipmapping, a technique that generates an optimized set of textures at different
        //      sizes. When generating the set of textures, OpenGL can use all of the texels to
        //      generate each level, ensuring that all of the texels wil also be used when filtering
        //      the texture. At render time, OpenGL will select the most appropriate level for each
        //      fragment based on the number of texels per fragment.
        //      With mipmapping, more memory will be used, but rendering can also be faster because
        //      the smaller levels take less space in the GPU's texture cache.
        //      With mipmaps enabled, OpenGL will select most appropriate texture level and then
        //      do bilinear interpolation using that optimized texture. Each level was built with
        //      information from all of the texels, so the resulting image looks much better, with
        //      much of the detail preserved.

        // Trilinear Filtering
        //      When we use mipmaps with bilinear filtering, we can sometimes see a noticeable jump
        //      or line in the rendered scene where OpenGL switches between different mipmap levels.
        //      We can switch to trilinear filtering to tell OpenGL to also interpolate between the
        //      two closest mipmap levels, using a total of eight texels per fragment. This helps
        //      to eliminate the transition between each mipmap level and results is a much smoother
        //      image.

        // Minification filter
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        // Magnification filter (bilinear filtering)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        // We can now load the bitmap data into OpenGL with an easy call to GLUtils
        texImage2D(GL_TEXTURE_2D, 0, bitmap, 0);
        bitmap.recycle();   // Recycle data immediately

        // Generate mipmaps
        glGenerateMipmap(GL_TEXTURE_2D);
        // Unbind to make no further changes
        glBindTexture(GL_TEXTURE_2D, 0);

        return textureObjectIds[0];
    }
}





















